-- MySQL Script
-- 16/09/2020 01:06:52
-- Version: 1.0

START TRANSACTION;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

-- -----------------------------------------------------
-- Schema Music
-- -----------------------------------------------------
DROP SCHEMA IF EXISTS `zero_brand`;
DROP USER IF EXISTS 'zero_brand'@'%';

-- -----------------------------------------------------
-- Schema Music
-- -----------------------------------------------------
CREATE SCHEMA IF NOT EXISTS `zero_brand` DEFAULT CHARACTER SET utf8 COLLATE utf8_general_ci;
CREATE USER 'zero_brand'@'%' IDENTIFIED BY 'zero_brand';
GRANT ALL PRIVILEGES ON `zero_brand`.* TO 'zero_brand'@'%';
FLUSH PRIVILEGES;

USE `zero_brand`;

-- -----------------------------------------------------
-- Table `zero_brand`.`node_tree`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zero_brand`.`node_tree`;

CREATE TABLE IF NOT EXISTS `zero_brand`.`node_tree` (
    `idNode` INT NOT NULL AUTO_INCREMENT,
    `level` INT NOT NULL,
    `iLeft` INT NOT NULL,
    `iRight` INT NOT NULL,
    PRIMARY KEY (`idNode`)
);

-- -----------------------------------------------------
-- Table `zero_brand`.`node_tree_names`
-- -----------------------------------------------------
DROP TABLE IF EXISTS `zero_brand`.`node_tree_names` ;

CREATE TABLE IF NOT EXISTS `zero_brand`.`node_tree_names` (
    `idNode` INT NOT NULL,
    `language` ENUM('english', 'italian') NOT NULL,
    `NodeName` VARCHAR(64) NOT NULL,
    FOREIGN KEY (`idNode`) REFERENCES `zero_brand`.`node_tree` (`idNode`)
);

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
COMMIT;