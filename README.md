# Backend Developer Assignment

Each business software has built-in support for organizational charts, in order to represent hierarchies and roles within a company. A common and convenient way to manage them with a tree structure in relational databases is the model **"[Nested Set](https://en.wikipedia.org/wiki/Nested_set_model)"** (click the link for more details).

Ogni software aziendale ha un supporto integrato per gli organigrammi, al fine di rappresentare gerarchie e ruoli all'interno di un'azienda. Un modo comune e conveniente per gestirli con una struttura ad albero nei database relazionali è il modello **"[Nested Set](https://en.wikipedia.org/wiki/Nested_set_model)"** (cliccare il link per ulteriori dettagli).

## Database structure (Struttura del Database)
The following MySQL tables contain an organization chart, along with the names of roles in various languages, normalized according to the **Nested Set** model.

Le seguenti tabelle MySQL contengono un organigramma, insieme ai nomi dei ruoli in varie lingue, normalizzate secondo il modello **Nested Set**.

**node_tree**

|idNode|level| iRight | iLeft |
|------|-----|--------|-------|
|   1  |  2  |    2   |   3   |
|   2  |  2  |    4   |   5   |
|   3  |  2  |    6   |   7   |
|   4  |  2  |    8   |   9   |
|   5  |  1  |    1   |  24   |
|   6  |  2  |   10   |  11   |
|   7  |  2  |   12   |  19   |
|   8  |  3  |   15   |  16   |
|   9  |  3  |   17   |  18   |
|  10  |  2  |   20   |  21   |
|  11  |  3  |   13   |  14   |
|  12  |  2  |   22   |  23   |

**node_tree_names (idNode foreign key node_tree.idNode)**

|idNode|language|NodeName           |
|------|--------|-------------------|
|   1  |english |Marketing          |
|   1  |italian |Marketing          |
|   2  |english |Helpdesk           |
|   2  |italian |Supporto Tecnico   |
|   3  |english |Managers           |
|   3  |italian |Manager            |
|   4  |english |Customer Account   |
|   4  |italian |Assistenza Cliente |
|   5  |english |0brand Srl         |
|   5  |italian |0brand Srl         |
|   6  |english |Accounting         |
|   6  |italian |Amministrazione    |
|   7  |english |Sales              |
|   7  |italian |Supporto Vendite   |
|   8  |english |Italy              |
|   8  |italian |Italia             |
|   9  |english |Europe             |
|   9  |italian |Europa             |
|  10  |english |Developers         |
|  10  |italian |Sviluppatori       |
|  11  |english |North America      |
|  11  |italian |Nord America       |
|  12  |english |Quality Assurance  |
|  12  |italian |Controllo Qualità  |

## Specification of requirements (Specifica dei requisiti)
A front end application needs to retrieve the organization chart nodes and show them in a tree view, and thus depends on a backend API to efficiently obtain that data.
The candidate is required to implement a php script, api.php, to return the nodes of the organization chart being able to specify which level to start from and allowing paging.
The script will be called via HTTP (GET method) through an Apache web server and will receive the following input parameters:

* node_id (integer, **mandatory**): the unique ID of the selected node.
* language (enum, **mandatory**): language identifier. Possible values: "english", "italian".
* search_keyword (string, **optional**): a search term used to filter the results. If provided, it limits the results in all child nodes under node_id whose nodeName in the given language contains the search keyword (not case sensitive).
* page_num (integer, **optional**): the 0-based identifier of the page to retrieve. If not specified, the default is 0.
* page_size (integer, **optional**): the size, in terms of records, of the page to retrieve, between 0 and 1000. If not provided, the default is 100.

The API should return a JSON with the following fields:

+ nodes (array, **mandatory**): 0 or more nodes that meet the following conditions. Each node must contain:
	* node_id (integer, **required**): the unique ID of the selected node.
	* name (string, **mandatory**): the name of the node translated into the requested language.
	* children_count (integer, **required**): the number of children of **this** node.
* next_page (integer, **optional**): the number of the next page if the records are more than page_size (only for Senior positions)
* prev_page (integer, **optional**): the number of the previous page if page_num is beyond page 0. (only for Senior positions)
* error (string, **optional**): if an error has occurred, fill in with the error message.

Un'applicazione front end deve recuperare i nodi dell'organigramma e mostrarli con una visualizzazione ad albero e, quindi, dipende da un'API di backend per ottenere in modo efficiente tali dati.
Al candidato è richiesto di implementare uno script php, api.php, per restituire i nodi dell'organigramma potendo specifica da quale livello partire e permettendo la paginazione.
Lo script verrà chiamato tramite HTTP (metodo GET) attraverso un server web Apache e riceverà i seguenti parametri di input:

* node_id (numero intero, **obbligatorio**): l'ID univoco del nodo selezionato.
* language (enum, **obbligatorio**): identificatore della lingua. Valori possibili: "english", "italian".
* search_keyword (stringa, **opzionale**): un termine di ricerca utilizzato per filtrare i risultati. Se fornito, limita i risultati in tutti i nodi figli sotto node_id il cui nodeName nella lingua data contiene la parola chiave di ricerca (senza distinzione tra maiuscole e minuscole).
* page_num (numero intero, **opzionale**): l'identificatore 0-based della pagina da recuperare. Se non specificato il valore predefinito è 0.
* page_size (numero intero, **opzionale**): la dimensione, in termini di record, della pagina da recuperare, compresa tra 0 e 1000. Se non fornito, il valore predefinito è 100.

L'API dovrebbe restituire un JSON con i seguenti campi:

+ nodes (array, **obbligatorio**): 0 o più nodi che rispettano le seguenti condizioni. Ogni nodo deve contenere:
	* node_id (integer, **obbligatorio**): l'ID univoco del nodo selezionato.
	* name (string, **obbligatorio**): il nome del nodo tradotto nel linguaggio richiesto.
	* children_count (integer, **obbligatorio**): il numero di figli di **questo** nodo.
* next_page (integer, **opzionale**): il numero della prossima pagina se i record sono più di page_size (solo per posizioni Senior)
* prev_page (integer, **opzionale**): il numero della pagina precedente se page_num è oltre la pagina 0. (solo per posizioni Senior)
* error (string, **opzionale**): se è avvenuto un errore compilare con il messaggio dell’errore.

## Constraints (Vincoli)
The proposed solution must check that all required parameters are passed and valid and return, in case of error, the following messages:

* "Invalid node ID" (if node_id is not found).
* "Missing required parameters" (if any required input parameters are not passed or have an empty value).
* "Invalid requested page number" (if page_num is not a valid 0-based number).
* "Invalid page size required" (if page_size is outside the validity range).
* No framework is allowed (**only for Senior and Mid positions**)
* The code provided must be PHP 7.3 compliant and use the "pdo_mysql" extension to query the database.
* Code quality is not optional: comments, clear and meaningful variable names and well structured and designed PHP code will be highly considered in the final evaluation of this test.

The candidate must provide a repository (**only for Senior and Mid, Junior can provide a compressed archive**) with the following "minimal" structure:

* api.php (entry point)
* config.php (configuration file containing database access credentials and any configurations) **for junior candidates using a framework this file will be the framework environment file**.
* tables.sql (SQL script for defining the tables) **for junior candidates using a framework they can also be migrations**.
* data.sql (SQL script for data insertion) **for junior candidates using a framework they can also be seeds**.

La soluzione proposta deve controllare che tutti i parametri richiesti siano passati e validi e restituisca, in caso di errore, i seguenti messaggi:

* "ID nodo non valido" (se node_id non viene trovato).
* "Parametri obbligatori mancanti" (se un qualsiasi parametro di input richiesto non viene passato o ha valore vuoto).
* "Numero di pagina richiesto non valido" (se page_num non è un numero 0-based valido).
* "Richiesto formato pagina non valido" (se page_size non rientra nell'intervallo di validità).
* Nessun framework è consentito (**solo per posizioni Senior e Mid**)
* Il codice fornito deve essere conforme a PHP 7.3 e utilizzare l'estensione "pdo_mysql" per eseguire query verso il database.
* La qualità del codice non è opzionale: commenti, nomi di variabili chiari e significativi e codice PHP ben strutturato e progettato sarà altamente considerato nella valutazione finale di questo test.

Il candidato deve fornire un repository (**solo per Senior e Mid, Junior possono fornire un archivio compresso**) con la seguente struttura "minima":

* api.php (punto di ingresso)
* config.php (file di configurazione contenente le credenziali di accesso al database ed eventuali configurazioni) **per candidati junior che utilizzano un framework questo file sarà il file di enviroment del framework**.
* tables.sql (script SQL di definizione delle tabelle) **per candidati junior che utilizzano un framework possono essere anche delle migration**.
* data.sql (script SQL per l'inserimento dei dati) **per candidati junior che utilizzano un framework possono essere anche dei seed**.

## Contributing
Pull requests are welcome. For major changes, please open an issue first to discuss what you would like to change.

Please make sure to update tests as appropriate.

## License
[MIT](https://choosealicense.com/licenses/mit/)