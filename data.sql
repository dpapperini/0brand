-- MySQL Script
-- 13/09/2020 01:06:52
-- Version: 1.0

START TRANSACTION;
SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0;
SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0;
SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO';

-- -----------------------------------------------------
-- Schema Music
-- -----------------------------------------------------
USE `zero_brand`;

-- -----------------------------------------------------
-- Insert Data `zero_brand`.`node_tree`
-- -----------------------------------------------------
TRUNCATE TABLE `zero_brand`.`node_tree`;
INSERT INTO node_tree(level, iLeft, iRight) VALUES
    (2, 2, 3),
    (2, 4, 5),
    (2, 6, 7),
    (2, 8, 9),
    (1, 1, 24),
    (2, 10, 11),
    (2, 12, 19),
    (3, 15, 16),
    (3, 17, 18),
    (2, 20, 21),
    (3, 13, 14),
    (2, 22, 23);

-- -----------------------------------------------------
-- Insert Data `zero_brand`.`node_tree_names`
-- -----------------------------------------------------
TRUNCATE TABLE `zero_brand`.`node_tree_names` ;

INSERT INTO node_tree_names(idNode, language, NodeName) VALUES
    (1, 'english', 'Marketing'),
    (1, 'italian', 'Marketing'),
    (2, 'english', 'Helpdesk'),
    (2, 'italian', 'Supporto Tecnico'),
    (3, 'english', 'Managers'),
    (3, 'italian', 'Manager'),
    (4, 'english', 'Customer Account'),
    (4, 'italian', 'Assistenza Cliente'),
    (5, 'english', '0brand Srl'),
    (5, 'italian', '0brand Srl'),
    (6, 'english', 'Accounting'),
    (6, 'italian', 'Amministrazione'),
    (7, 'english', 'Sales'),
    (7, 'italian', 'Supporto Vendite'),
    (8, 'english', 'Italy'),
    (8, 'italian', 'Italia'),
    (9, 'english', 'Europe'),
    (9, 'italian', 'Europa'),
    (10, 'english', 'Developers'),
    (10, 'italian', 'Sviluppatori'),
    (11, 'english', 'North America'),
    (11, 'italian', 'Nord America'),
    (12, 'english', 'Quality Assurance'),
    (12, 'italian', 'Controllo Qualità');

SET SQL_MODE=@OLD_SQL_MODE;
SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS;
SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS;
COMMIT;