<?php

namespace ZeroBrand;

use PDO;
use Exception;

class Api
{
    // pdo and config
    protected static $pdo;
    private $configs;

    // api args
    public $node_id;
    public $language;
    public $search_keyword;
    public $page_num;
    public $page_size;

    // pagination
    protected $totalRow;
    protected $offset;
    protected $prev_page;
    protected $next_page;

    // response
    protected $response;
    protected $response_code;
    protected $nodes;
    protected $errors;

    public function __construct()
    {
        // set configs
        $this->configs = require_once('config.php');
        $this->totalRow = null;
        $this->offset = null;
        $this->response_code = null;

        // set connect to db
        try {
            self::$pdo = new PDO(
                $this->configs['driver'].":host=".$this->configs['host'].";port=".$this->configs['port'].";dbname=".$this->configs['database'],
                $this->configs['username'],
                $this->configs['password']
            );
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        } catch (PDOException $e) {
            echo "Error: " . $e->getMessage();
            die();
        }
        
        // get input data
        $this->getInputData();

        // send response
        $this->sendResponse();
    }

    // check if idNode exists in db
    private function nodeExists()
    {
        $count = 0;
        try {
            $sql = "SELECT COUNT(*) FROM node_tree WHERE idNode = :node_id";
            $stmt = self::$pdo->prepare($sql);
            $stmt->execute([ ':node_id' => $this->node_id ]);
            $count = (int) $stmt->fetchColumn();
        } catch(PDOException $e) {
            echo "Error: " . $e->getMessage();
            die();
        }
        return $count ? true : false;
    }

    // return data from db
    private function getNodes()
    {
        // set pagination params
        $this->page_num = isset($_GET['page_num']) ? $this->page_num : $this->page_num+1;
        $this->offset = ($this->page_num-1) * $this->page_size;
        $this->prev_page = $this->page_num - 1;
        $this->next_page = $this->page_num + 1;

        // set sql query
        $sql = "SELECT nodes.idNode AS node_id, tc.NodeName AS name,
            (SELECT count(*) FROM node_tree AS count_node, node_tree AS count_parent
            WHERE count_node.idNode <> count_parent.idNode AND count_parent.idNode = nodes.idNode
            AND count_node.iLeft BETWEEN count_parent.iLeft AND count_parent.iRight) AS children_count
        FROM node_tree AS nodes LEFT JOIN node_tree_names AS tc ON nodes.idNode = tc.idNode, node_tree AS parent
        WHERE parent.idNode = :node_id AND nodes.iLeft BETWEEN parent.iLeft AND parent.iRight
        AND tc.language = :language AND tc.NodeName LIKE :search_keyword
        ORDER BY nodes.iLeft";

        try {

            // get total row 
            $stmt = self::$pdo->prepare($sql);
            $stmt->execute([ ':language' => $this->language, ':search_keyword' => '%'.$this->search_keyword.'%', ':node_id' => $this->node_id ]);
            $this->totalRow = (int) $stmt->rowCount();

            // get result
            $stmt = self::$pdo->prepare($sql . " LIMIT $this->offset, $this->page_size");
            $stmt->execute([ ':language' => $this->language, ':search_keyword' => '%'.$this->search_keyword.'%', ':node_id' => $this->node_id ]);
            $rows = $stmt->fetchAll(PDO::FETCH_ASSOC);

        } catch (PDOException $e) {

            echo "Error: " . $e->getMessage();
            die();

        }

        return $rows;
    }

    // get input data from $_GET and if not found set error
    private function getInputData()
    {
        // check if request method is GET
        if ($_SERVER['REQUEST_METHOD'] === 'GET') {
            // filter sanitize input       
            $this->node_id = (int) trim(filter_input(INPUT_GET, 'node_id', FILTER_SANITIZE_NUMBER_INT));
            $this->language = trim(filter_input(INPUT_GET, 'language', FILTER_SANITIZE_STRING));
            $this->search_keyword = trim(filter_input(INPUT_GET, 'search_keyword', FILTER_SANITIZE_STRING));
            $this->page_num = (int) trim(filter_input(INPUT_GET, 'page_num', FILTER_SANITIZE_NUMBER_INT));
            $this->page_size = (int) trim(filter_input(INPUT_GET, 'page_size', FILTER_SANITIZE_NUMBER_INT));
            $this->errors = null;
            $this->response_code = 400;

            // check and verify field required
            if (!$this->node_id || (!$this->language || $this->language)) {            
                
                if (!in_array($this->language, ['english', 'italian'])) {
                    $this->errors[] = "Parametri obbligatori mancanti";
                }

            }
            
            // check if idNode exists
            if (!$this->node_id) {
                $this->errors[] = "ID nodo non valido";
            } else {
                if (isset($_GET['node_id']) && (!$this->nodeExists() || !is_numeric($_GET["node_id"]))) {
                    $this->errors[] = "ID nodo non valido";
                    $this->response_code = 404;
                }
            }

            // check if page_num is set and 0-based
            if (isset($_GET["page_num"]) && ($this->page_num <= 0 || !is_numeric($_GET["page_num"]))) {
                $this->errors[] = "Numero di pagina richiesto non valido";
            }       

            // check if page_size range 0 to 1000
            if (isset($_GET["page_size"]) && (!in_array($this->page_size, range(0, 1000)) || !is_numeric($_GET["page_size"]))) {
                $this->errors[] = "Richiesto formato pagina non valido";
            } else {
                $this->page_size = !isset($_GET["page_size"]) ? 100 : $this->page_size;
            }
        } else {
            // custo errors when method not allowed
            $this->errors[] = "Method Not Allowed";
            $this->response_code = 405;
        }
    }
    
    // send response
    private function sendResponse()
    {
        // set error or set response
        if (!is_null($this->errors)) {
            $this->response = [
                'nodes' => [],
                'error' => implode(", ", $this->errors)
            ];
        } else {
            $this->response_code = 200;
            $this->response = [
                'nodes' => $this->getNodes()
            ];
            
            if ($this->prev_page && $this->page_size && $this->prev_page < ceil($this->totalRow/$this->page_size)) {
                $this->response['prev_page'] = $this->prev_page;
            } else {
                if ($this->page_size && $this->prev_page !== 0) {
                    $this->errors[] = "Numero di pagina richiesto non valido";
                    $this->response['error'] = implode(", ", $this->errors);
                    $this->response_code = 400;
                }
            }

            if ($this->page_size && $this->page_num < ceil($this->totalRow/$this->page_size)) {
                $this->response['next_page'] = $this->next_page;
            }
        }

        // set header and return response
        header('Content-type: application/json;charset=utf-8');
        http_response_code($this->response_code);
        echo json_encode($this->response);
        die;
    }

}

$Api = new Api();